# pypolynom

The **pypolynom** project is for educational purpose.
It provides a simple project for the practical sessions of a software engineering training (See https://github.com/silx-kit/silx-training/tree/master/software_engineering).

## Installation

Simply run: `pip install pypolynom`

## Documentation

The documentation is available here: https://silx.gitlab-pages.esrf.fr/pypolynom_completed

## Testing

Continuous integration:

- GitLab-CI: [![GitLab-CI Status](https://gitlab.esrf.fr/silx/pypolynom_completed/badges/main/pipeline.svg)](https://gitlab.esrf.fr/silx/pypolynom_completed/pipelines)

To test the project locally, run `pytest` in source directory.

## License

The source code of this project is licensed under the MIT license.
See the [LICENSE](https://gitlab.esrf.fr/silx/trainingproject_completed/blob/master/LICENSE) file.
