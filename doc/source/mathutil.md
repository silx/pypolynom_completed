# {mod}`mathutil`: mathutil

```{eval-rst}
.. automodule:: pypolynom.mathutil
    :members: sqrt, pow2
```
