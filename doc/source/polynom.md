# {mod}`polynom`: Polynom

```{eval-rst}
.. automodule:: pypolynom.polynom
    :members: polynom
```
