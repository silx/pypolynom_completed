# {mod}`gui`: gui

```{eval-rst}
.. automodule:: pypolynom.gui
    :members: PolynomSolver
```
