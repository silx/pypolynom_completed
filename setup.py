# coding: utf-8
"""Project setup script

See documentation and sample setup.py from the Python Packaging Authority(PyPA) for more information:

- https://setuptools.pypa.io/en/latest
- https://github.com/pypa/sampleproject/blob/master/setup.py
"""

import setuptools
from setuptools.extension import Extension

setuptools.setup(
    ext_modules=[Extension("pypolynom.cpolynom", ["pypolynom/cpolynom.pyx"])],
)
