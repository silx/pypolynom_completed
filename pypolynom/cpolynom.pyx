# coding: utf-8

"""This is a simple demonstration library"""

__authors__ = [
    "Loïc Huder",
    "Pierre Knobel",
    "Jerome Kieffer",
    "Pierre Paleo",
    "Henri Payno",
    "Marius Retegan",
    "Armando Sole",
    "Valentin Valls",
    "Thomas Vincent",
]
__date__ = "15/10/2021"
__license__ = "MIT"


def polynom(a, b, c):
    """Solve the quadratic equation.

    .. math:: a\cdotx^2 + b\cdotx + c = 0

    :param float a: a value of the polynom
    :param float b: b value of the polynom
    :param float c: c value of the polynom
    :rtype: List[float]
    """
    if a == 0:
        # Not a second-degree polynomial.
        raise ValueError("Not a quadratic equation if a = 0")
    delta = (b ** 2.0) - 4.0 * a * c
    solutions = []
    if delta > 0:
        solutions.append((-b + (delta ** 0.5)) / (2.0 * a))
        solutions.append((-b - (delta ** 0.5)) / (2.0 * a))
    elif delta == 0:
        solutions.append(-b / (2.0 * a))
    return solutions
