# coding: utf-8

"""This is a simple demonstration library"""

__authors__ = [
    "Loïc Huder",
    "Pierre Knobel",
    "Jerome Kieffer",
    "Pierre Paleo",
    "Henri Payno",
    "Marius Retegan",
    "Armando Sole",
    "Valentin Valls",
    "Thomas Vincent",
]
__date__ = "15/10/2021"
__license__ = "MIT"


def sqrt(x):
    """Return the square root of x.

    :param float x: Input value
    :rtype: float
    """
    return x ** 0.5


def pow2(x):
    """Return the square of x.

    :param float x: input value
    :rtype: float
    """
    return x * x
