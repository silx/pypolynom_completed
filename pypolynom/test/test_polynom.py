# coding: utf-8

"""Unit tests for the module polynom."""

import pytest
import math
from .. import polynom


@pytest.fixture(
    params=[
        ([4, 1, 3], []),
        ([1.2, 6.5, 1.0], [-5.25818, -0.15848]),
        ([9, 3.3, -100], [-3.52170, 3.15503]),
        ([-9.11, -8, 10.0], [-1.57507, 0.69691]),
    ]
)
def polynom_data(request):
    return request.param


class TestPolynom:
    @pytest.mark.parametrize(
        "coefs, nroots", [([2, 0, 1], 0), ([2, 0, 0], 1), ([4, 0, -4], 2)]
    )
    def test_number_of_roots(self, coefs, nroots):
        assert len(polynom.polynom(*coefs)) == nroots

    def test_roots(self):
        result = polynom.polynom(math.pi, math.pi, -math.pi)
        result = sorted(result)
        assert result[0] == pytest.approx(-1.6180, 1e-4)
        assert result[1] == pytest.approx(0.6180, 1e-4)

    def test_many_roots(self, polynom_data):
        coefs, roots = polynom_data
        result = polynom.polynom(*coefs)
        result = sorted(result)
        for i in range(len(roots)):
            assert result[i] == pytest.approx(roots[i], 1e-4)

    def test_not_polynom(self):
        with pytest.raises(ValueError):
            polynom.polynom(0, 0, 0)
