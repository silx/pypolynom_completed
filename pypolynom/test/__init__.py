"""
Test module.

.. code-block::

    # Test the project.
    python -m pytest

    # Test coverage with percentage of coverage per module.
    python -m pytest --cov 
    # or
    python -m coverage run -m pytest
    python -m coverage report


    # Test coverage with file annotation.
    python -m pytest --cov --cov-report annotate
    # or
    python -m coverage annotate
"""
