# coding: utf-8

"""Unit tests for the module math."""

import pytest
from .. import mathutil


class TestSqrt:
    def test_integer(self):
        result = mathutil.sqrt(4)
        assert result == 2

    def test_float(self):
        result = mathutil.sqrt(0.5)
        assert pytest.approx(result - 0.7071, 1e-4)

    def test_string(self):
        with pytest.raises(TypeError):
            mathutil.sqrt("mmm")


class TestPow2:
    def test_no_roots(self):
        result = mathutil.pow2(2)
        assert result == 4
